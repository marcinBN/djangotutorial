#-*- coding: utf-8 -*-

from socket import *
import select
import re




def broadcast(sock, msg, channel):
    print 'sending ' + msg
    print 'from ' + clientNames[sock.getpeername()]
    print 'to ', channel
    for s in channel:
        if s is not sock and s is not serverSocket:
            try:
                s.send(msg)
            except:
                s.close()
                conn_list.remove(s)    

def getname(clientSocket):
    clientSocket.send('Podaj imię: ')
    
    data = clientSocket.recv(BUFF)
    return data.rstrip()
   
def chooseChannel(clientSocket, channel):
    channels.setdefault(channel, []).append(clientSocket)
    channels['mainChannel'].remove(clientSocket)
    clientChannels[clientSocket.getpeername()] = channel

if __name__ == '__main__':
    HOST = ''
    PORT = 21567
    ADDR = (HOST, PORT)
    BUFF = 4096
    
    serverSocket = socket(AF_INET, SOCK_STREAM)
    serverSocket.bind((HOST, PORT))
    serverSocket.listen(10)
    connList = [serverSocket]
    channels = {'mainChannel':[]}
    clientNames = {}
    clientChannels = {}
    
    while 1:
        readSockets, writeSockets, errSockets = \
           select.select(connList, [], [])
        for socket in readSockets:
            if socket == serverSocket:
                #new connection
                clientSocket, addr = serverSocket.accept()
                connList.append(clientSocket)
                channels['mainChannel'].append(clientSocket)
                clientChannels[clientSocket.getpeername()] = 'mainChannel'
                print addr, ' connected'
                clientName = getname(clientSocket)
                clientNames[clientSocket.getpeername()] = clientName
                broadcast(clientSocket, clientName + ' dołączył\r', channels['mainChannel'])
                
            else:
                #data received from client
                data = socket.recv(BUFF)
                if data:
                    if re.match(r'\/join \#(\w+)', data):
                        ch = re.match(r'\/join \#(\w+)', data).group(1)
                        chooseChannel(socket, ch)
                        print channels
                        print clientChannels
                    else:
                        print 'hura'
                        broadcast(socket, '\r<' + clientNames[socket.getpeername()] + '> ' + data, channels[clientChannels[socket.getpeername()]])
    serverSocket.close()
            
